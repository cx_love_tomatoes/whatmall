package com.cxcool.whatmall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatmallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhatmallWareApplication.class, args);
    }

}
