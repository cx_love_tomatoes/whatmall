package com.cxcool.whatmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhatmallMemberApplication.class, args);
    }

}
