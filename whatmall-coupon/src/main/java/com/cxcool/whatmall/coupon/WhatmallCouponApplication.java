package com.cxcool.whatmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhatmallCouponApplication.class, args);
    }

}
