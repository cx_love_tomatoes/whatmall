package com.cxcool.whatmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhatmallOrderApplication.class, args);
    }

}
