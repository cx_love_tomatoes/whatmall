package com.cxcool.whatmall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhatmallProductApplication.class, args);
    }

}
